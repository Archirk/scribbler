import shutil
from pathlib import Path

import ffmpeg
import pytest
from PIL import Image

from src.project import Project
from tests import test_vars as tv


@pytest.fixture(scope="session")
def mock_image() -> Path:
    i = Image.new("L", (2, 2))
    i.save(tv.MOCK_RECORDING_IMAGE_PATH)
    p = Path(tv.MOCK_RECORDING_IMAGE_PATH)
    yield p
    p.unlink(missing_ok=True)


@pytest.fixture(scope="session")
def mock_recording(mock_image: Path) -> Path:
    img = ffmpeg.input(mock_image.absolute(), loop=1, t=tv.MOCK_RECORDING_DURATION, framerate=1)
    ffmpeg.output(img, tv.MOCK_RECORDING_PATH, r=1).global_args("-y").global_args("-loglevel", "warning").run()
    p = Path(tv.MOCK_RECORDING_PATH)
    yield p
    p.unlink(missing_ok=True)


@pytest.fixture(autouse=True, scope="function")
def test_workdir() -> Path:
    p = Path("./_test_workdir")
    p.mkdir(exist_ok=True)
    yield p
    shutil.rmtree(p.absolute())


@pytest.fixture(scope="function")
def project_dir(test_workdir: Path) -> Path:
    Project.init(test_workdir)
    p = test_workdir / Project._NAME
    yield p
    shutil.rmtree(p.absolute())


@pytest.fixture(scope="function")
def unit_dir(project_dir: Path) -> Path:
    p = Project(project_dir)
    u = p.new_unit(tv.TEST_UNIT_NAME, tv.TEST_DATE)
    yield u.path
    shutil.rmtree(u.path.absolute())


@pytest.fixture(scope="function")
def add_mock_recording(unit_dir: Path, mock_recording: Path):
    shutil.copy(mock_recording.absolute(), (unit_dir / f"recording.{mock_recording.name.split(".")[-1]}").absolute())


@pytest.fixture(scope="function")
def add_single_result(unit_dir: Path, mock_image: Path):
    shutil.copy(mock_image.absolute(), (unit_dir / "result" / mock_image.name).absolute())
