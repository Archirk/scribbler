from pathlib import Path

import pytest
import yaml
from click.exceptions import ClickException

from src import models as m
from src.cli.app import build, close, init, new
from tests import test_vars as tv


def test_init_handler(test_workdir: Path):
    init(test_workdir)
    project_dir = test_workdir / "Scribbler"
    assert project_dir.exists()
    for c in tv.PROJECT_EXPECTED_CONTENT:
        assert (project_dir / c).exists()
    with pytest.raises(FileExistsError):
        init(test_workdir)


def test_new_handler(project_dir: Path):
    new(tv.TEST_UNIT_NAME, tv.TEST_DATE, project_dir=project_dir)
    unit_dir = project_dir / "units" / f"{tv.TEST_DATE}-{tv.TEST_UNIT_NAME.replace(" ", "_")}"
    assert unit_dir.exists()
    for c in tv.UNIT_EXPECTED_CONTENT:
        assert (unit_dir / c).exists()

    data: dict = yaml.load((unit_dir / "meta.yaml").read_bytes(), yaml.SafeLoader)
    assert data.pop("name", None) == tv.TEST_UNIT_NAME
    assert data.pop("date", None) == tv.TEST_DATE
    assert len(data) == 0, f"There is unexpected items in meta.yaml {data}"

    with pytest.raises(FileExistsError):
        new(tv.TEST_UNIT_NAME, tv.TEST_DATE, project_dir=project_dir)


def test_close_handler_no_results(unit_dir: Path):
    pdir = unit_dir.parent.parent
    with pytest.raises(ClickException):
        close(unit_dir.name, 3, 2, m.SessionType.CONTROL, m.Mediums.PHOTOSHOP, comment="Hehe", project_dir=pdir)


def test_close_handler_ok(add_mock_recording, add_single_result, unit_dir: Path, project_dir: Path):
    close(unit_dir.name, 3, 2, m.SessionType.CONTROL, m.Mediums.PHOTOSHOP, comment="Hehe", project_dir=project_dir)
    data: dict = yaml.load((unit_dir / "meta.yaml").read_bytes(), yaml.SafeLoader)
    assert data.pop("name", None) == tv.TEST_UNIT_NAME
    assert data.pop("date", None) == tv.TEST_DATE
    assert data.pop("satisfaction", None) == 3
    assert data.pop("stage", None) == 2
    assert data.pop("session_type", None) == m.SessionType.CONTROL
    assert data.pop("medium", None) == m.Mediums.PHOTOSHOP
    assert data.pop("comment", None) == "Hehe"
    assert data.pop("duration_sec", None) == tv.MOCK_RECORDING_DURATION
    assert len(data) == 0, f"There is unexpected items in meta.yaml {data}"


@pytest.mark.skip("Can mock compose timelapse call")
def test_build_handler_ok(monkeypatch, add_mock_recording, add_single_result, unit_dir: Path, project_dir: Path):
    close(unit_dir.name, 3, 2, m.SessionType.CONTROL, m.Mediums.PHOTOSHOP, comment="Hehe", project_dir=project_dir)
    build(unit_dir.name, project_dir=project_dir)
    data: dict = yaml.load((unit_dir / "meta.yaml").read_bytes(), yaml.SafeLoader)

    assert "public" in data
    public: dict = data["public"]
    assert public.pop("title", None) == data["name"]
    assert public.pop("description", None) == data["comment"]
    assert public.pop("is_mature_content", None) is False
    assert len(public) == 0, f"There is unexpected items in meta.yaml-public {public}"

    assert (unit_dir / "public" / "outro.png").exists()
    assert (unit_dir / "public" / "thumbnail.png").exists()
    assert (unit_dir / "public" / "result.jpg").exists()
    assert (unit_dir / "timelapse.mp4").exists()
