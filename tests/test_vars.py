TEST_UNIT_NAME = "Pytest Unit"
TEST_DATE = "20201201"
PROJECT_EXPECTED_CONTENT = ("assets", "units", "config.yaml")
UNIT_EXPECTED_CONTENT = ("public", "result", "references", "meta.yaml")
MOCK_RECORDING_PATH = "./_mock-1h.mkv"
MOCK_RECORDING_IMAGE_PATH = "./_mock_img.png"
MOCK_RECORDING_DURATION = 3600
