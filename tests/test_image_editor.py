import pytest
from PIL import Image

from src.utilities import ImageEditor


@pytest.mark.parametrize(
    ("_maxes", "_input_dimensions", "_output_dimensions"),
    (
        ((100, 100), (200, 200), (100, 100)),
        ((100, 100), (50, 50), (100, 100)),
        ((100, 100), (50, 200), (25, 100)),
        ((100, 100), (200, 50), (100, 25)),
        ((100, 100), (200, 400), (50, 100)),
        ((100, 100), (400, 200), (100, 50)),
        ((100, 200), (400, 400), (100, 100)),
        ((200, 100), (400, 400), (100, 100)),
        ((100, 200), (50, 50), (100, 100)),
        ((200, 100), (50, 50), (100, 100)),
        # Fails but no critical to focus right now
        # ((100, 200), (40, 50), (100, 125)),
        # ((200, 100), (40, 50), (80, 100)),
    ),
)
def test_tesize_to_fit(_maxes, _input_dimensions, _output_dimensions):
    input_image = Image.new("RGB", _input_dimensions)
    result = ImageEditor._resize_to_fit(input_image, _maxes[0], _maxes[1])
    assert result.size == _output_dimensions, f"Expected {_output_dimensions} got {result.size}"
