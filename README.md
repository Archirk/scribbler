# ScribblerCLI
Personal CLI tool for documenting and publishing progress in drawing.

## Installation
```bash
# Developed under 3.12 so... a lot of problems with dependecies - be ready.
sudo apt install python3.12-distutils
curl -sS https://bootstrap.pypa.io/get-pip.py |  python3.12
python3.12 -m pip install -U setuptools
python3.12 -m pip install -qUI git+https://gitlab.com/Archirk/scribbler.git@main
```

## Quickstart
##### 1. Initilize and enter project directory
```bash
scribbler init && cd Scribbler
```
##### 2. Modify config ./config.yaml
For publishing it is required to have:
- Telegram bot token and chat_id to publish units
- Youtube API user credentials with `"https://www.googleapis.com/auth/youtube"` scope

##### 3. Manage project with interactive CLI
```bash
scribbler --help

CLI to manage and publish Project Scribbler                                    
                                                                                
╭─ Options ────────────────────────────────────────────────────────────────────╮
│ --install-completion          Install completion for the current shell.      │
│ --show-completion             Show completion for the current shell, to copy │
│                               it or customize the installation.              │
│ --help                        Show this message and exit.                    │
╰──────────────────────────────────────────────────────────────────────────────╯
╭─ Commands ───────────────────────────────────────────────────────────────────╮
│ build    Build assets from existing materials for publishing (e.g.           │
│          thumbnails, timelapse and so on)                                    │
│ close    Close session by providing experiment info and saving it to meta.   │
│ init     Initializes new scribbler project                                   │
│ new      Create new experiment unit directory with premade files and         │
│          directories.                                                        │
│ publish  Publish unit to available and enabled platforms                     │
╰──────────────────────────────────────────────────────────────────────────────╯
```
