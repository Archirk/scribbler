import typing as t
from pathlib import Path

import yaml

from src import models as m
from src.exceptions import ScribblerException


class Unit:
    """
    Unit represents single session instance of Project Scribbler experiment
    """

    meta: m.Meta
    path: Path

    __result_dir = "result"
    __reference_dir = "references"
    __public_dir = "public"
    __result_name = "result.jpg"
    __thumbnail_name = "thumbnail.png"
    __recording_name = "recording.mkv"
    __timelapse_name = "timelapse.mp4"
    __outro_name = "outro.png"
    __meta = "meta.yaml"

    def __init__(self, meta: m.Meta):
        self.meta = meta

    @property
    def name(self) -> str:
        return self.meta.name

    @property
    def result_dir(self) -> Path:
        return self.path / self.__result_dir

    @property
    def public_dir(self) -> Path:
        return self.path / self.__public_dir

    @property
    def recording(self) -> Path:
        return self.path / self.__recording_name

    @property
    def timelapse(self) -> Path:
        return self.path / self.__timelapse_name

    @property
    def result(self) -> Path:
        return self.public_dir / self.__result_name

    @property
    def thumbnail(self) -> Path:
        return self.public_dir / self.__thumbnail_name

    @property
    def outro(self) -> Path:
        return self.public_dir / self.__outro_name

    @property
    def meta_path(self) -> Path:
        return self.path / self.__meta

    @classmethod
    def from_dir(cls, path: Path) -> t.Self:
        with (path / cls.__meta).open() as f:
            meta_dict = yaml.safe_load(f)

        unit = cls(m.Meta.model_validate(meta_dict))
        unit.path = path
        return unit

    def create(self, *, units_dir: Path) -> None:
        self.path = units_dir / f"{self.meta.date}-{self.name.replace(" ", "_")}"
        self.path.mkdir()
        self.flush_meta()

        for d in self.__result_dir, self.__reference_dir, self.__public_dir:
            (self.path / d).mkdir()

    def flush_meta(self) -> None:
        with self.meta_path.open("w") as outfile:
            yaml.dump(
                self.meta.model_dump(mode="json", exclude_none=True),
                outfile,
                indent=2,
                sort_keys=False,
                encoding="utf-8",
            )

    def get_results(self) -> list[Path]:
        filter = {"jpeg", "png", "jpg", "svg"}
        return [f for f in self.result_dir.iterdir() if str(f).split(".")[-1] in filter]

    def update_publish_info(self, info: t.Optional[m.TelegramPublishInfo | m.YoutubePublishInfo]) -> None:
        if info is None:
            return
        if self.meta.public.publishing_info is None:  # type: ignore
            self.meta.public.publishing_info = m.PublishingInfo()  # type: ignore
        match t := type(info):
            case m.TelegramPublishInfo:
                self.meta.public.publishing_info.telegram = info  # type: ignore
            case m.YoutubePublishInfo:
                self.meta.public.publishing_info.youtube = info  # type: ignore
            case _:
                raise ScribblerException(f"Unknown publishing target: '{t}'")
        self.flush_meta()

    def is_published_to(self, target: t.Literal["telegram", "youtube"]) -> bool:
        if self.meta.public.publishing_info is None:  # type: ignore
            return False
        match target:
            case "telegram":
                return bool(self.meta.public.publishing_info.telegram)  # type: ignore
            case "youtube":
                return bool(self.meta.public.publishing_info.youtube)  # type: ignore
            case _:
                raise ScribblerException(f"Unknown publishing target: '{target}'")
