import typing as t
from datetime import datetime
from pathlib import Path

import yaml

from src import models as m
from src.exceptions import ProjectException
from src.publishers import TelegramPublisher, YoutubePublisher
from src.utilities import ImageEditor, VideoEditor

from .unit import Unit


def duration_str(duration_sec: int) -> str:
    return "{}h {}m".format(*divmod(divmod(duration_sec, 60)[0], 60))


class Project:
    """
    Represents project which contains units, assets and other related stuff
    """

    path: Path
    config: m.Config
    image_editor: ImageEditor
    video_editor: VideoEditor

    _telegram: TelegramPublisher
    _youtube: YoutubePublisher
    _NAME = "Scribbler"
    DATE_FORMAT = "%Y%m%d"
    MAX_SATISFACTION = 5
    __assets_dir = "assets"
    __unit_dir = "units"
    __config = "config.yaml"

    def __init__(self, project_dir: Path):
        self.path = project_dir
        self.config = m.Config.model_validate(yaml.safe_load(self.config_path.read_bytes()))
        self.image_editor = ImageEditor(self.config.image_editor)
        self.video_editor = VideoEditor(self.config.video_editor)
        self._telegram = TelegramPublisher(self.config.publisher.telegram)
        self._youtube = YoutubePublisher(self.config.publisher.youtube)

    @property
    def assets(self) -> Path:
        return self.path / self.__assets_dir

    @property
    def units(self) -> Path:
        return self.path / self.__unit_dir

    @property
    def config_path(self) -> Path:
        return self.path / self.__config

    @classmethod
    def init(cls, init_dir: Path) -> None:
        project_path = init_dir / cls._NAME
        project_path.mkdir()
        (project_path / cls.__assets_dir).mkdir()
        (project_path / cls.__unit_dir).mkdir()
        (project_path / cls.__config).write_text(
            yaml.dump(
                m.Config().model_dump(mode="json"),
                indent=2,
                sort_keys=False,
            )
        )

    @classmethod
    def list_units(cls) -> t.Generator[Path, None, None]:
        """Tries to return list of units. Return empty if wrong workdir"""
        p = Path(".") / cls.__unit_dir
        if not p.exists():
            return None
        for path in filter(lambda x: x.is_dir(), p.iterdir()):
            yield path

    @classmethod
    def _validate_date_format(cls, date: str) -> None:
        try:
            datetime.strptime(date, cls.DATE_FORMAT)
        except Exception:
            raise ProjectException(f"Invalid format date for unit. Format {cls.DATE_FORMAT} expected.")

    @classmethod
    def _validate_name(cls, name: str) -> None:
        if len(name) > ImageEditor.MAX_NAME_LENGTH:
            raise ProjectException(f"Max name length is {ImageEditor.MAX_NAME_LENGTH}")

    @classmethod
    def _validate_comment(cls, comment: str) -> None:
        if len(comment) > ImageEditor.MAX_COMMENT_LENGTH:
            raise ProjectException(f"Max name length is {ImageEditor.MAX_COMMENT_LENGTH}")

    def new_unit(self, name: str, date: str) -> Unit:
        self._validate_name(name)
        self._validate_date_format(date)
        u = Unit(m.Meta(name=name, date=date))
        u.create(units_dir=self.units)
        return u

    def get_unit(self, dir_name: str) -> Unit:
        return Unit.from_dir(self.units / dir_name)

    def finish_unit(
        self,
        unit: Unit,
        *,
        satisfaction: int,
        stage: int,
        session_type: m.SessionType,
        medium: m.Mediums,
        comment: str,
        duration: int,
    ) -> None:
        self._validate_comment(comment)
        unit.meta.satisfaction = satisfaction
        unit.meta.stage = stage
        unit.meta.session_type = session_type
        unit.meta.medium = medium
        unit.meta.comment = comment
        unit.meta.duration_sec = duration
        unit.flush_meta()

    def get_duration(self, unit: Unit) -> int:
        if not unit.recording.exists():
            return 0
        return self.video_editor.get_duration_sec(unit.recording)

    @staticmethod
    def _ensure_existance(*files: Path) -> None:
        missing = []
        for f in files:
            if not f.exists():
                missing.append(str(f.absolute()))
        if len(missing) != 0:
            raise ProjectException(f"Required files are missing:\n {'\n'.join(missing)}")

    def build_public_assets(self, unit: Unit, force: bool) -> None:
        if unit.result.exists() and not force:
            print("Result image already exists. Skipping image generation")
            return
        self.image_editor.compose_result(*unit.get_results(), output=unit.result)
        self.image_editor.make_title_card(
            output=unit.thumbnail,
            preview_img=unit.result,
            title=unit.name,
            sub_title=(f"{unit.meta.session_type.capitalize()}" f" session {unit.meta.date}"),  # type: ignore
            satisfaction=f"{unit.meta.satisfaction}/{self.MAX_SATISFACTION}",
            duration=duration_str(unit.meta.duration_sec),  # type: ignore
        )
        self.image_editor.make_outro_card(
            output=unit.outro,
            preview_img=unit.result,
            text=unit.meta.comment,  # type: ignore
        )

    def compose_timelapse(self, unit: Unit, force: bool) -> None:
        if unit.timelapse.exists() and not force:
            print("Timelapse already exists. Skipping")
            return
        self._ensure_existance(unit.recording, unit.thumbnail, unit.outro)
        sound = self.config.video_editor.timelapse.intro_sound_path
        if sound:
            self._ensure_existance(sound)
        self.video_editor.compose_timelapse(
            input_video=unit.recording,
            output_path=unit.timelapse,
            title_card_path=unit.thumbnail,
            outro_card_path=unit.outro,
            title_sound_path=sound,
        )

    def publish(self, unit: Unit, only_validate: bool) -> None:
        self._telegram.validate(unit)
        self._youtube.validate(unit)
        if only_validate:
            print("Seems like everything is ok for publishing")
            return
        calls = (self._telegram.publish, self._youtube.publish)
        for c in calls:
            publisher_name = c.__qualname__.split(".")[0]
            try:
                print(f"{publisher_name} is publishing...")
                info = c(unit)
            except Exception as exc:
                print(f"Failed to publish with {publisher_name}: {exc}")
                continue
            try:
                unit.update_publish_info(info)
            except Exception as exc:
                print(f"Failed to store publishing info {info}: {exc}")
