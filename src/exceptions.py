

class ScribblerException(Exception):
    ...


class PublisherException(ScribblerException):
    ...


class ProjectException(ScribblerException):
    ...
