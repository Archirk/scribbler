import typing as t
from pathlib import Path

import typer
from typing_extensions import Annotated

from src import models as m
from src.project import Project


def complete_units() -> list[str]:
    for u in Project.list_units():
        yield u.name


argUnitPath = Annotated[str, typer.Argument(help="Unit name", show_default=False, autocompletion=complete_units)]
argProjectDir = Annotated[Path, typer.Argument(help="Directory where project will be created", show_default=True)]

optName = Annotated[
    str,
    typer.Option(
        help="Human readable name that is used for storing and publishing",
        show_default=False,
        prompt=True,
    ),
]
optDate = Annotated[str, typer.Option(help="Date of session in YYYYMMDD format", prompt=True)]
optStage = Annotated[
    int,
    typer.Option(
        help="Global group for batch of experiments",
        show_default=False,
        prompt=True,
    ),
]
optMedium = Annotated[
    m.Mediums,
    typer.Option(
        help="Main tool or software used within session",
        show_default=False,
        prompt=True,
    ),
]
optComment = Annotated[str, typer.Option(help="Comment to session. Short observation", prompt=True)]
optSessionType = Annotated[m.SessionType, typer.Option(help="Session type", show_default=False, prompt=True)]
optProjectDir = Annotated[Path, typer.Option(help="Project directory", show_default=True)]
optForceBuild = Annotated[bool, typer.Option(help="Force building assets", show_default=True)]
optSatisfactionLevel = Annotated[
    int,
    typer.Option(
        ...,
        help="Satisfaction level from session",
        show_default=False,
        max=Project.MAX_SATISFACTION,
        prompt=True,
    ),
]
optTitle = Annotated[t.Optional[str], typer.Option(help="Title for public use.", prompt=True)]
optDescription = Annotated[t.Optional[str], typer.Option(help="Description for public use.", prompt=True)]
optMatureContent = Annotated[bool, typer.Option(help="Flag 18+ content", prompt=True)]
optValidate = Annotated[bool, typer.Option(help="Only validate without any actions")]
