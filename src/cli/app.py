from datetime import datetime
from pathlib import Path

import typer
from click.exceptions import ClickException
from src import models as m
from src.project import Project

from . import inputs as i

app = typer.Typer(
    help="CLI to manage and publish Project Scribbler",
    add_completion=True,
    pretty_exceptions_show_locals=False,
)


@app.command()
def init(init_dir: i.argProjectDir = Path(".")) -> None:
    """
    Initializes new scribbler project
    """
    Project.init(init_dir)
    print("Project created. Don't forget to update config before start")


@app.command()
def new(
    name: i.optName,
    date: i.optDate = datetime.today().strftime(Project.DATE_FORMAT),
    project_dir: i.optProjectDir = Path("."),
) -> None:
    """
    Create new experiment unit directory with premade files and directories.
    """
    Project(project_dir).new_unit(name, date)


@app.command()
def close(
    unit_name: i.argUnitPath,
    satisfaction: i.optSatisfactionLevel,
    stage: i.optStage = 0,
    session_type: i.optSessionType = m.SessionType.TRAINING,
    medium: i.optMedium = m.Mediums.KRITA,
    comment: i.optComment = "No comments",
    project_dir: i.optProjectDir = Path("."),
) -> None:
    """
    Close session by providing experiment info and saving it to meta.
    """
    project = Project(project_dir)
    unit = project.get_unit(unit_name)
    if len(unit.get_results()) == 0:
        raise ClickException("Can't close unit without any results")
    duration = project.get_duration(unit)
    if duration == 0:
        while True:
            prompt = input("Seems no recording provided. Enter spent time manually (in minutes):")
            try:
                duration = int(prompt) * 60  # type: ignore
                break
            except ValueError:
                print("Integer is expected")

    project.finish_unit(
        unit,
        satisfaction=satisfaction,
        stage=stage,
        session_type=session_type,
        medium=medium,
        comment=comment,
        duration=duration,
    )
    return


@app.command()
def build(
    unit_name: i.argUnitPath,
    title: i.optTitle = "",
    description: i.optDescription = "",
    force: i.optForceBuild = False,
    mature_content: i.optMatureContent = False,
    project_dir: i.optProjectDir = Path("."),
) -> None:
    """
    Build assets from existing materials for publishing (e.g. thumbnails, timelapse and so on)
    """
    project = Project(project_dir)
    unit = project.get_unit(unit_name)
    unit.meta.public = m.Public(
        title=title if title else unit.name,
        description=description if description else unit.meta.comment,
        is_mature_content=mature_content,
    )
    unit.flush_meta()
    project.build_public_assets(unit, force)
    print("Composing timelapse...")
    project.compose_timelapse(unit, force)


@app.command()
def publish(
    unit_name: i.argUnitPath, only_validate: i.optValidate = False, project_dir: i.optProjectDir = Path(".")
) -> None:
    """
    Publish unit to available and enabled platforms
    """
    project = Project(project_dir)
    unit = project.get_unit(unit_name)
    project.publish(unit, only_validate)
