import math
import typing as t
from pathlib import Path

import ffmpeg

from src.models import VideoEditorConfig


class VideoEditor:
    _config: VideoEditorConfig

    __FRAMERATE = 30

    def __init__(self, config: VideoEditorConfig):
        self._config = config

    @classmethod
    def get_duration_sec(cls, video_path: Path) -> int:
        data = ffmpeg.probe(video_path.absolute())
        return math.ceil(float(data["format"]["duration"]))

    @classmethod
    def get_frame_count(cls, video_path: Path) -> int:
        # For now - very unreliable
        data = ffmpeg.probe(video_path.absolute())
        duration = cls.get_duration_sec(video_path)
        for stream in data["streams"]:
            if stream.get("codec_type") == "video":
                try:
                    return int(stream["nb_frames"])
                except KeyError:
                    framerate = float(stream["r_frame_rate"].split("/")[0])
                    return int(duration * framerate)
        raise Exception("No video streams found in video")

    def compose_timelapse(
        self,
        *,
        input_video: Path,
        output_path: Path,
        title_card_path: Path,
        outro_card_path: Path,
        title_sound_path: t.Optional[Path],
    ) -> None:
        # All calculations done to achive that timelapse length is constant
        duration_hours = self.get_duration_sec(input_video) // 3600 + 1
        timelapse_frame_count = duration_hours * (self._config.timelapse.minute_per_hour * 60 * self.__FRAMERATE)
        intro_frame_count = self._config.timelapse.intro_card_duration * self.__FRAMERATE
        outro_frame_count = self._config.timelapse.outro_card_duration * self.__FRAMERATE
        timelapse_frame_count -= intro_frame_count + outro_frame_count

        framestep = self.get_frame_count(input_video) // timelapse_frame_count

        timelapse = ffmpeg.input(input_video)
        timelapse = ffmpeg.filter(timelapse, "framestep", framestep)
        timelapse = ffmpeg.filter(timelapse, "setpts", f"N/{self.__FRAMERATE}/TB")

        intro = ffmpeg.input(
            title_card_path.absolute(),
            loop=1,
            t=self._config.timelapse.intro_card_duration,
            framerate=self.__FRAMERATE,
        )
        outro = ffmpeg.input(
            outro_card_path.absolute(),
            loop=1,
            t=self._config.timelapse.outro_card_duration,
            framerate=self.__FRAMERATE,
        )
        timelapse = ffmpeg.concat(intro, timelapse, outro)

        output_streams = [timelapse]

        if title_sound_path:
            # Manual tweeking. # Expected to be 2 channel audio
            delay_ms = 4500
            intro_sound = ffmpeg.input(title_sound_path.absolute())
            intro_sound = ffmpeg.filter(intro_sound, "adelay", f"{delay_ms}|{delay_ms}")
            output_streams.append(intro_sound)

        # ffmpeg output bindig fails to process otherway
        output = str(output_path.absolute())
        result = (
            ffmpeg.output(*output_streams, output, r=self.__FRAMERATE)
            .global_args("-y")
            .global_args("-loglevel", "warning")
        )

        ffmpeg.run(result)
