import textwrap
from pathlib import Path

from PIL import Image, ImageDraw, ImageFont

from src.models import ImageEditorConfig


class ImageEditor:
    _config: ImageEditorConfig

    __CARD_WIDTH = 1920
    __CARD_HEIGHT = 1080
    __CARD_PADDING = 50
    __FONT_FAMILY = "FiraCode_Regular.ttf"
    MAX_NAME_LENGTH = 25
    MAX_COMMENT_LENGTH = 18 * 30

    def __init__(self, config: ImageEditorConfig):
        self._config = config

    @staticmethod
    def _resize_to_fit(img: Image.Image, max_w: int, max_h: int) -> Image.Image:
        size, sampling = img.size, Image.LANCZOS
        if size[0] == size[1]:
            m = min(max_w, max_h)
            return img.resize((m, m), sampling)

        if size[0] > size[1]:
            scale_factor = max_w / float(size[0])
            return img.resize((max_w, int(size[1] * scale_factor)), sampling)

        scale_factor = max_h / float(size[1])
        new_size = (int(size[0] * scale_factor), max_h)
        return img.resize(new_size, sampling)

    def _new_preset_image(self) -> Image.Image:
        return Image.new(
            "RGB",
            (self.__CARD_WIDTH, self.__CARD_HEIGHT),
            self._config.thumbnail.background_color,
        )

    def make_title_card(
        self,
        *,
        output: Path,
        preview_img: Path,
        title: str,
        sub_title: str,
        duration: str,
        satisfaction: str,
    ) -> None:
        result = self._new_preset_image()
        lines = (
            (title, 120, (self.__CARD_PADDING, 50)),
            (sub_title, 100, (self.__CARD_PADDING, 200)),
            (
                f"Duration: {duration}",
                80,
                (self.__CARD_PADDING, self.__CARD_HEIGHT - 260),
            ),
            (
                f"Satisfaction: {satisfaction}",
                80,
                (self.__CARD_PADDING, self.__CARD_HEIGHT - 150),
            ),
        )
        for text, size, pos in lines:
            text_line = ImageDraw.Draw(result)
            text_line.text(
                pos,
                text,
                fill=self._config.thumbnail.text_color,
                font=ImageFont.truetype(self.__FONT_FAMILY, size=size),
                stroke_width=0,
                stroke_fill=self._config.thumbnail.text_stroke_color,
            )

        preview = Image.open(preview_img.absolute())
        # Size depends on text font and size Tweek manually.
        preview = self._resize_to_fit(preview, 900, 650)
        result.paste(
            preview,
            (
                self.__CARD_WIDTH - self.__CARD_PADDING - preview.size[0],
                self.__CARD_HEIGHT - self.__CARD_PADDING - preview.size[1],
            ),
        )
        result.save(output.absolute())

    def make_outro_card(self, *, output: Path, preview_img: Path, text: str) -> None:
        result = self._new_preset_image()
        text_params = dict(
            xy=(self.__CARD_PADDING, self.__CARD_PADDING),
            text=textwrap.fill(text, width=30),
            font=ImageFont.truetype(self.__FONT_FAMILY, size=50),
            stroke_width=1,
        )
        text_draw = ImageDraw.Draw(result)
        text_right_x = text_draw.textbbox(**text_params)[2]

        # For some reason textbox does not accept this params so
        # have to add them separately.
        text_params.update(
            fill=self._config.thumbnail.text_color,
            stroke_fill=self._config.thumbnail.text_stroke_color,
        )
        text_draw.text(**text_params)

        if preview_img:
            preview = Image.open(preview_img.absolute())
            max_w, max_h = (
                self.__CARD_WIDTH - text_right_x - 2 * self.__CARD_PADDING,
                self.__CARD_HEIGHT - 2 * self.__CARD_PADDING,
            )
            preview = self._resize_to_fit(preview, max_w, max_h)
            x, y = preview.size
            center_aligned_y_start = int((self.__CARD_HEIGHT - y) / 2)
            x_offset = text_right_x
            center_aligned_x_start = int(x_offset + (self.__CARD_WIDTH - x_offset - x) / 2)
            result.paste(preview, (center_aligned_x_start, center_aligned_y_start))
        result.save(output.absolute())

    def compose_result(self, *img_path: Path, output: Path) -> None:
        match len(img_path):
            case 0:
                raise Exception("No images to compose result")
            case 1:
                output.write_bytes(img_path[0].read_bytes())
            case _:
                item_min_w, item_min_h, max_item_per_line = 400, 400, 5
                images = [Image.open(p) for p in img_path]
                images.reverse()
                items = [self._resize_to_fit(i, item_min_w, item_min_h) for i in images]
                locations = []
                column, row = 0, 0
                for i in items:
                    x, y = column * i.size[0], row * i.size[1]
                    locations.append((x, y))
                    if column == max_item_per_line - 1:
                        column = 0
                        row += 1
                        continue
                    column += 1

                result_w = (
                    item_min_w * max_item_per_line if len(img_path) >= max_item_per_line else item_min_w * len(img_path)
                )
                result_h = (len(img_path) // max_item_per_line + 1) * item_min_h
                result = Image.new("RGB", (result_w, result_h), color="#888888")
                for img, location in zip(items, locations):
                    result.paste(img, location)
                result.save(output.absolute())
