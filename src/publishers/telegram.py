import time
import typing as t

import requests

from src import models as m
from src.exceptions import PublisherException
from src.models.config import TelegramData
from src.project.unit import Unit


class TelegramPublisher:
    _data: TelegramData
    __MAX_VIDEO_SIZE = 50 << 20

    def __init__(self, publisher_data: TelegramData):
        self._data = publisher_data

    @property
    def _url(self) -> str:
        return f"https://api.telegram.org/bot{self._data.token}"

    def _build_message(self, unit: Unit) -> str:
        lines = [f"*{unit.meta.public.title}*"]  # type: ignore
        if d := unit.meta.public.description:  # type: ignore
            lines.append(d)
        return "\n".join(lines)

    def validate(self, unit: Unit) -> None:
        if not self._data.enable:
            return
        if not self._data.token:
            raise PublisherException("Token for Telegram is not provided")
        video_path = unit.timelapse
        if video_exists := video_path.exists():
            # Sad that we will read file twice: here and during publishing. But whatever for now.
            if size := len(video_path.read_bytes()) >= self.__MAX_VIDEO_SIZE:
                raise PublisherException(
                    f"Video to big for telegram: {size >> 20}mb (max:{self.__MAX_VIDEO_SIZE << 20}mb)"
                )
        if not video_exists and not unit.result.exists():
            raise PublisherException("Both video and image are missing. Nothing to publish to telegram")

    def _delete_in_comment_group(self, msg_id: int) -> None:
        if not self._data.comment_chat:
            print("No comment chat in config. Can't delete anything")
            return
        r = requests.get(f"{self._url}/getUpdates")
        if r.status_code != 200:
            raise PublisherException(f"Failed to get updates: {r.text}")
        # Not many updates is expected so just lurk through all 24hour update
        # message is optional field in update
        # message_id is optional field in forward_origin
        for forward_update in filter(
            lambda update: "message" in update and "forward_origin" in update["message"], r.json()["result"]
        ):
            if forward_update["message"]["forward_origin"].get("message_id") == msg_id:
                msg_to_delete = forward_update["message"]["message_id"]
                r = requests.get(
                    f"{self._url}/deleteMessage",
                    params=dict(chat_id=self._data.comment_chat, message_id=msg_to_delete),  # type: ignore
                )
                if r.status_code != 200:
                    raise PublisherException(f"Request failed: {r.text}")
                return
        raise PublisherException("Could find msg id to delete")

    def publish(self, unit: Unit) -> t.Optional[m.TelegramPublishInfo]:
        if not self._data.enable:
            print("Publishing to Telegram is disabled and will be skipped")
            return None
        if unit.is_published_to("telegram"):
            print("Unit already has been published to Telegram and will be skipped")
            return None

        video_path = unit.timelapse
        info = m.TelegramPublishInfo()
        data = dict(
            chat_id=self._data.chat,
            caption=self._build_message(unit)
            .replace(".", "\\.")
            .replace("(", "\\(")
            .replace(")", "\\)")
            .replace("-", "\\"),
            parse_mode="MarkdownV2",
        )
        if video_path.exists():
            video_raw = video_path.read_bytes()
            r = requests.post(f"{self._url}/sendVideo", data=data, files=dict(video=video_raw))
            if r.status_code != 200:
                raise PublisherException(f"Failed to publish video to Telegram: {r.text}")
            result = r.json()["result"]
            vmsg_id = result["message_id"]
            info.video_id = result["video"]["file_id"]
            info.video_msg_id = vmsg_id
            # If video published drop caption to not repeat ourself when sending result image
            try:
                time.sleep(5)  # just safe delay in case telegram is not swift enough
                self._delete_in_comment_group(vmsg_id)
            except Exception as exc:
                print(f"Failed to delete video msg in comment group: {exc}")

            data.pop("caption", None)

        if unit.result.exists():
            img_raw = unit.result.read_bytes()
            r = requests.post(f"{self._url}/sendPhoto", data=data, files=dict(photo=img_raw))
            if r.status_code != 200:
                raise PublisherException(f"Failed to publish result image to Telegram: {r.text}")
            result = r.json()["result"]
            info.img_msg_id = result["message_id"]
            info.img_id = result["photo"][0]["file_id"]
        return info
