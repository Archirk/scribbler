import os
import typing as t
from string import Template

import google_auth_oauthlib.flow
import googleapiclient.discovery
import googleapiclient.errors
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from googleapiclient.http import MediaFileUpload

from src import models as m
from src.exceptions import PublisherException
from src.project.unit import Unit


class YoutubePublisher:
    _config: m.YoutubeData
    _credentials: Credentials = None
    _youtube = None
    _title_template = Template("$text [$session_type session] - $medium - $date")

    __SERVER_PORT = 24999
    __CACHED_AUTH_PATH = "auth.json"
    __APP_NAME = "youtube"
    __API_VERSION = "v3"
    __scopes = ["https://www.googleapis.com/auth/youtube"]
    # __DAILY_QUOTA = 10000
    # __UPLOAD_COST = 1600
    # __THUMBNAIL_COST = 50
    # __COST_PER_PLAYLIST = 50

    def __init__(self, config: m.YoutubeData) -> None:
        self._config = config

    def authorize(self) -> None:
        user_creds = self._config.user_credentials
        auth_creds = user_creds.parent / self.__CACHED_AUTH_PATH
        if not auth_creds.exists():
            os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"
            flow = google_auth_oauthlib.flow.InstalledAppFlow.from_client_secrets_file(
                user_creds.absolute(), self.__scopes
            )
            auth = flow.run_local_server(port=self.__SERVER_PORT)
            auth_creds.write_text(auth.to_json())
        self._credentials = Credentials.from_authorized_user_file(auth_creds.absolute())

        if not self._credentials.has_scopes(self.__scopes):
            print("Scopes for youtube differ. Removing and retrying from scratch")
            auth_creds.unlink()
            self.authorize()

        if self._credentials.expired:
            self._credentials.refresh(Request())
            auth_creds.write_text(self._credentials.to_json())

        self._youtube = googleapiclient.discovery.build(
            self.__APP_NAME, self.__API_VERSION, credentials=self._credentials
        )

    def validate(self, unit: Unit) -> None:
        if not self._config.enable:
            return
        if not all((unit.timelapse.exists(), unit.thumbnail.exists())):
            raise PublisherException("Thumbnail or video is missing for publishing to yotube")
        if not unit.meta.public.title:  # type: ignore
            raise PublisherException("Empty title is not acceptable for youtube")
        if not self._config.user_credentials.exists():
            raise PublisherException("User credentails for youtube are missing")
        try:
            self.authorize()
        except Exception as exc:
            raise PublisherException(f"Failed to authorize youtube client: {exc}")

    def _build_title(self, unit: Unit) -> str:
        return self._title_template.substitute(
            text=unit.meta.public.title.capitalize(),  # type: ignore
            session_type=str(unit.meta.session_type).capitalize(),
            date=unit.meta.date,
            medium=unit.meta.medium,
        )

    def _build_description(self, unit: Unit) -> t.Optional[str]:
        lines = [unit.meta.public.description]  # type: ignore
        if self._config.signature:
            lines.append(self._config.signature)
        return "\n".join(lines)  # type: ignore

    def _upload_video(self, unit: Unit) -> str:
        upload_data = dict(
            part="snippet",
            media_body=MediaFileUpload(unit.timelapse.absolute()),
            body={
                "snippet": {
                    "title": self._build_title(unit),
                    "description": self._build_description(unit),
                    "status": {"privacyStatus": "public"},
                },
            },
        )
        upload_request = self._youtube.videos().insert(**upload_data)  # type: ignore
        r = upload_request.execute()
        video_id = r["id"]
        return video_id

    def _set_thumbnail(self, unit: Unit, video_id: str) -> None:
        thumb_request = self._youtube.thumbnails().set(  # type: ignore
            videoId=video_id, media_body=MediaFileUpload(unit.thumbnail.absolute())
        )
        thumb_request.execute()

    def _suggest_playlists(self, unit: Unit) -> list[m.YoutubePlaylists]:
        playlists = []
        match unit.meta.session_type:
            case m.SessionType.TRAINING:
                playlists.append(m.YoutubePlaylists.TRAINING_SESSIONS)
            case m.SessionType.DRAWING:
                playlists.append(m.YoutubePlaylists.DRAWING_SESSIONS)
            case m.SessionType.CONTROL:
                playlists.append(m.YoutubePlaylists.CONTROL_SESSIONS)
        match unit.meta.stage:
            case 0:
                playlists.append(m.YoutubePlaylists.STAGE_ZERO)
        lc_name = unit.name.lower()
        if any(
            (
                "gesture" in lc_name,
                "figure" in lc_name,
                "pose" in lc_name,
            )
        ):
            playlists.append(m.YoutubePlaylists.GESTURE_DRAWING)
        elif "scribble" in lc_name:
            playlists.append(m.YoutubePlaylists.SCRIBBLES)

        return playlists

    def _set_playlists(self, unit: Unit, video_id: str) -> None:
        for p in self._suggest_playlists(unit):
            playlist_request = self._youtube.playlistItems().insert(  # type: ignore
                part="snippet",
                body={
                    "snippet": {
                        "playlistId": p,
                        "resourceId": {"videoId": video_id, "kind": "youtube#video"},
                    },
                },
            )
            playlist_request.execute()

    def publish(self, unit: Unit) -> t.Optional[m.YoutubePublishInfo]:
        if not self._config.enable:
            print("Youtube publishing is disabled and will be skipped")
            return None
        if unit.is_published_to("youtube"):
            print("Unit already has been published to Youtube and will be skipped")
            return None
        if unit.meta.public.is_mature_content:  # type: ignore
            print("Unit contains mature content so YouTube upload is skipped")
            return None
        video_id = self._upload_video(unit)
        info = m.YoutubePublishInfo()
        info.video_id = video_id
        try:
            self._set_thumbnail(unit, video_id)
            info.thumbnail_is_set = True
        except Exception as exc:
            info.thumbnail_is_set = False
            print(f"Failed to set thumbnail: {exc}")

        try:
            self._set_playlists(unit, video_id)
        except Exception as exc:
            print(f"Failed to add video to all playlists: {exc}")

        return info
