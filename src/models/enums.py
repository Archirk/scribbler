from enum import StrEnum


class SessionType(StrEnum):
    TRAINING = "training"
    DRAWING = "drawing"
    CONTROL = "control"


class Mediums(StrEnum):
    PHOTOSHOP = "Photoshop"
    KRITA = "Krita"


class YoutubePlaylists(StrEnum):
    STAGE_ZERO = "PLDxFiLKdScpdBzMJjrt9BKWQKBoQ-IzMH"
    CONTROL_SESSIONS = "PLDxFiLKdScpfkuGqg8VyOltgg4A7oQn3G"
    TRAINING_SESSIONS = "PLDxFiLKdScpeg1P5OGQ23h51WZ39eEpfM"
    DRAWING_SESSIONS = "PLDxFiLKdScpeKVNu6tft8VuaR3vf4K2n1"
    SCRIBBLES = "PLDxFiLKdScpdzVi9IBgrr1zOtpO2RuYNI"
    GESTURE_DRAWING = "PLDxFiLKdScpde4eHhhYnseTCNPg3eQubX"
    LIFE_DRAWINGS = "PLDxFiLKdScpcG6uLpdAWD4pau8MH9QJy1"
    LIFE_DRAWINGS_MONITOR = "PLDxFiLKdScpfrsUnujUUCR8-IdPRVUq_V"
