import typing as t

from . import enums
from .base import BaseModel


class TelegramPublishInfo(BaseModel):
    video_id: t.Optional[str] = None
    video_msg_id: t.Optional[int] = None
    img_id: t.Optional[str] = None
    img_msg_id: t.Optional[int] = None


class YoutubePublishInfo(BaseModel):
    video_id: t.Optional[str] = None
    thumbnail_is_set: t.Optional[bool] = None


class PublishingInfo(BaseModel):
    telegram: t.Optional[TelegramPublishInfo] = None
    youtube: t.Optional[YoutubePublishInfo] = None


class Public(BaseModel):
    """
    Public containes information useful for publishing purpouse.
    """

    title: str
    description: t.Optional[str] = None
    is_mature_content: bool = False
    publishing_info: t.Optional[PublishingInfo] = None


class Meta(BaseModel):
    """
    Meta store unit facts and public info
    """
    
    name: str
    date: str
    stage: t.Optional[int] = None
    session_type: t.Optional[enums.SessionType] = None
    medium: t.Optional[enums.Mediums] = None
    duration_sec: t.Optional[int] = None
    comment: t.Optional[str] = None
    satisfaction: t.Optional[int] = None
    public: t.Optional[Public] = None
