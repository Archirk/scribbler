import typing as t
from pathlib import Path

from .base import BaseModel


class TimelapseOptions(BaseModel):
    minute_per_hour: int = 1
    intro_card_duration: int = 5
    outro_card_duration: int = 5
    intro_sound_path: t.Optional[Path] = None


class ThumbnailOptions(BaseModel):
    background_color: str = "#000000"
    text_color: str = "#ffffff"
    text_stroke_color: str = "#aa0000"


class VideoEditorConfig(BaseModel):
    timelapse: TimelapseOptions = TimelapseOptions()


class ImageEditorConfig(BaseModel):
    thumbnail: ThumbnailOptions = ThumbnailOptions()


class TelegramData(BaseModel):
    enable: bool = False
    token: str = ""
    chat: str = ""
    comment_chat: str = ""


class YoutubeData(BaseModel):
    enable: bool = False
    user_credentials: Path = Path("./creds.json")
    signature: t.Optional[str] = None


class PublisherConfig(BaseModel):
    telegram: TelegramData = TelegramData()
    youtube: YoutubeData = YoutubeData()


class Config(BaseModel):
    video_editor: VideoEditorConfig = VideoEditorConfig()
    image_editor: ImageEditorConfig = ImageEditorConfig()
    publisher: PublisherConfig = PublisherConfig()
